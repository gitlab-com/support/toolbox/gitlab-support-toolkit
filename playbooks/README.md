# Provisioning with Ansible

Ansible scripts to allow provisioning and management of GitLab related services on digital ocean instances 
or any Ubuntu server with SSH access.

## Requirements:
[Ansible 2.2+](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

Then install Ansible dependencies using `ansible-galaxy`:

```
ansible-galaxy install --role-file=../requirements.yml
```

## Ansible extra vars

Extra vars are environment variables passed to the ansible playbook command with the `-e` parameter.

| Extra vars   | Required             | Format       | Allowed Values       | URL |
| :----------- | :------------------- | :----------- | :------------------- | :---|
| omnibus      | no 	    	      | Boolean      | true                 |     |
| version      | when omnibus is true | x.x.x        | n.n+.n               | https://about.gitlab.com/releases/ |
| edition      | when omnibus is true | alphanumeric | ce or ee             | https://about.gitlab.com/releases/ |
| squid_proxy  | no                   | alphanumeric | docker tag (version) | https://hub.docker.com/r/sameersbn/squid/tags/ |
| gitlab-runner| no                   | alphanumeric | docker tag (version) | https://hub.docker.com/r/gitlab/gitlab-runner/tags/ |
| elasticsearch| no                   | alphanumeric | docker tag (version) | https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html |

## Managing your instances

Several parameters are mandatory when using `main.yml`.

| Parameter     | Description                        |
| :------------ | :--------------------------------- |
| --private-key | Private key to connect to instance |
| -i            | Hostname or IP of instance         | 
| -e            | Extra vars to be included          |

Note: Ansible allows these values to persist in [config](https://docs.ansible.com/ansible/latest/reference_appendices/config.html) 
and other places such as playbooks however this is out of scope of this document.

#### Example commands 

Note: `main.yml` is the default playbook. See [Ansible Inventory](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html) 
for best practice over a large number of hosts (especially useful for Geo/HA).

## Provision specific version of Omnibus

```
ansible-playbook --private-key ~/.ssh/private_key \
                 -i gitlab.example.com, main.yml \
                 -e "omnibus=true \
                     version=11.0.1 edition=ee \
                     EXTERNAL_URL=http://gitlab.example.com"

```

## Provision instance with a containerised GitLab Runner

```
ansible-playbook --private-key ~/.ssh/private_key \
                 -i gitlab-runner.example.com, main.yml \
                 -e "gitlab_runner=v11.0.0"
```

## Provision GitLab Runner, Squid Proxy and Elasticsearch on a standalone host

```
ansible-playbook --private-key ~/.ssh/private_key \
                 -i gitlab-services.example.com, main.yml \
                 -e "gitlab-runner=v11.0.0 \
                     squid_proxy=latest \
                     elasticsearch=5.5.0"
```

## Building your own module / customization

Before writing your own note that most common services (i.e. Openldap, Jenkins etc) 
are available via Ansible-Galaxy [here](https://galaxy.ansible.com/).

To write your own module:

1. Checkout a new branch

2. Generate folder tree:
`mkdir -p playbooks/{module_name}/{tasks,vars,handlers}`

2. Generate playbooks:
`touch playbooks/{module_name}/{tasks,vars,handlers}/main.yml`

3. Take and modify snippets from existing modules or refer to the [Ansible](http://docs.ansible.com/ansible) docs to create your module.

4. If you wish for the module to be compatible with Vagrant then update the `e_vars` hash in the Vagrantfile.

5. Send merge request.
